"use strict";

const db = require("../models");
const Voluntario = db.voluntarios;

// Recupera todos los Voluntarios de la BD.
exports.findAll = (req, res) => {

  Voluntario.findAll()
      .then(voluntarios => {
        res.send(voluntarios);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Algunos errores ocurrieron recuperando voluntarios."
        });
      });
};