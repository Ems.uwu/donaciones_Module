"use strict";

const db = require("../models");
const Donacion = db.donaciones;

// Crea y Guarda una nueva Donacion.
exports.create = (req, res) => {
    
  // Valida la solicitud
    if (!req.body.voluntario_id) {
      res.status(400).send({
        message: "El contenido no puede estar vacío!"
      });
      return;
    }
  
    // Crea una Donacion
    const donacion = {
      voluntarioId: req.body.voluntario_id,
      tipo_ayuda: req.body.tipo_ayuda,
      descripcion: req.body.descripcion,
      fecha: Date.now()
    };
  
    // Guarda una Donacion en la BD
    Donacion.create(donacion)
      .then(() => {
        res.status(200).send({
          message: "Nuevo registro Donacion ha sido creado."
        });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Algunos errores ocurrieron creando la Donacion."
        });
      });
};