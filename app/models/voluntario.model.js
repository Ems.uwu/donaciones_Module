"use strict";

module.exports = (sequelize, Sequelize) => {
    const Voluntario = sequelize.define("voluntarios", {
      id: {
        type: Sequelize.INTEGER, unique:true, 
        allowNull: false, autoIncrement: true, 
        primaryKey: true
      },
      user_id: {
        type: Sequelize.INTEGER, unique:true,
        allowNull: false
      },
      nombre: {
        type: Sequelize.STRING(30), allowNull: false
      },
      apellido: {
        type: Sequelize.STRING(30), allowNull: false
      },
      cedula: {
        type: Sequelize.STRING(10), allowNull: false
      },
      edad: {
        type: Sequelize.SMALLINT(4), allowNull: false
      },
      sexo: {
        type: Sequelize.STRING(1), allowNull: false
      },
      tipo_sangre: {
        type: Sequelize.STRING(3), allowNull: false
      },
      donante_valido: {
        type: Sequelize.BOOLEAN, allowNull: false
      },
      tipo_voluntario: {
        type: Sequelize.STRING(10), allowNull: false
      },
      nro_celular: {
        type: Sequelize.STRING(15), allowNull: false
      },
      nro_fijo: {
        type: Sequelize.STRING(15), allowNull: false
      },
      direccion: {
        type: Sequelize.STRING(100), allowNull: false
      },
      email: {
        type: Sequelize.STRING(30), allowNull: false
      },
      facebook_usrname: {
        type: Sequelize.STRING(30), allowNull: true
      },
      instagram_usrname: {
        type: Sequelize.STRING(30), allowNull: true
      }
    },
    {
      timestamps: false
    }
    
    );

    Voluntario.associate = function(models) {
      Voluntario.hasMany(models.donaciones);
    };
    
    return Voluntario;
  };
