"use strict";

module.exports = (sequelize, Sequelize) => {
    const Donacion = sequelize.define("donaciones", {
      id: {
        type: Sequelize.INTEGER, unique: true, 
        allowNull: false, autoIncrement: true, 
        primaryKey: true
      },
      tipo_ayuda: {
        type: Sequelize.STRING(30), allowNull: false
      },
      descripcion: {
        type: Sequelize.STRING(75), allowNull: false
      },
      fecha: {
        type: Sequelize.DATE, allowNull: false
      }
    },
      {
        timestamps: false
      }
    
    );

    Donacion.associate = function(models) {
      Donacion.belongsTo(models.voluntarios/*, {
        foreignKey: {
          name: 'voluntario_id',
          unique: true, allowNull: false
        }
      }*/);
    };

    return Donacion;
  };

