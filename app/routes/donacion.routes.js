"use strict";

module.exports = app => {
    const donaciones = require("../controllers/donacion.controller");
  
    var router = require("express").Router();
  
    // Crea una nueva instancia de Donaciones
    router.post("/donacion/new", donaciones.create);

    app.use('/api', router);

};