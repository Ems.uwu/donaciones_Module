"use strict";

module.exports = app => {
    const voluntario = require("../controllers/voluntario.controller");

    var router = require("express").Router();

    // Recupera todos los Voluntarios
    router.get("/voluntarios", voluntario.findAll);

    app.use('/api', router);

};