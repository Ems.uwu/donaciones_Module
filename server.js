"use strict";

const express = require('express');
const cors = require("cors");
const db = require("./app/models");

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(express.json());

var corsOptions = {
    origin: "http://localhost:8081"
}
  
app.use(cors(corsOptions));

require("./app/routes/voluntario.routes")(app);
require("./app/routes/donacion.routes")(app);

db.sequelize.sync();

app.listen(app.get('port'), (req, res) => {

    console.log('Server up!');
} );